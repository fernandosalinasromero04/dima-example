<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perros extends Model
{
    use HasFactory;
    protected $fillable = ['nombre', 'raza', 'edad', 'es_favorito'];

    public function usuarios()
    {
        return $this->belongsToMany(User::class, 'perros_favoritos')->withTimestamps();
    }
}
