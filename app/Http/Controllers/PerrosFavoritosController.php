<?php

namespace App\Http\Controllers;

use App\Models\PerrosFavoritos;
use App\Http\Requests\StorePerrosFavoritosRequest;
use App\Http\Requests\UpdatePerrosFavoritosRequest;

class PerrosFavoritosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePerrosFavoritosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePerrosFavoritosRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PerrosFavoritos  $perrosFavoritos
     * @return \Illuminate\Http\Response
     */
    public function show(PerrosFavoritos $perrosFavoritos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PerrosFavoritos  $perrosFavoritos
     * @return \Illuminate\Http\Response
     */
    public function edit(PerrosFavoritos $perrosFavoritos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePerrosFavoritosRequest  $request
     * @param  \App\Models\PerrosFavoritos  $perrosFavoritos
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePerrosFavoritosRequest $request, PerrosFavoritos $perrosFavoritos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PerrosFavoritos  $perrosFavoritos
     * @return \Illuminate\Http\Response
     */
    public function destroy(PerrosFavoritos $perrosFavoritos)
    {
        //
    }
}
