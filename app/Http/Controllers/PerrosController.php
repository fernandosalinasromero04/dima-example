<?php

namespace App\Http\Controllers;

use App\Models\Perros;
use App\Http\Requests\StorePerrosRequest;
use App\Http\Requests\UpdatePerrosRequest;
use Illuminate\Http\Request;

class PerrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perros = Perros::all();
        return inertia('Perros/Index', compact('perros'));
    }

    public function marcarFavorito(Request $request)
    {
        $dog = Perros::findOrFail($request->perro['id']);
        if($request->perro['es_favorito'] == 0){
            $dog->es_favorito = true;
        } else {
            $dog->es_favorito = false;
        }
        $dog->save();

        $usuario = $request->user();
        $usuario->perrosFavoritos()->toggle($dog);
        return to_route('perros.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePerrosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePerrosRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Perros  $perros
     * @return \Illuminate\Http\Response
     */
    public function show(Perros $perros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Perros  $perros
     * @return \Illuminate\Http\Response
     */
    public function edit(Perros $perros)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePerrosRequest  $request
     * @param  \App\Models\Perros  $perros
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePerrosRequest $request, Perros $perros)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Perros  $perros
     * @return \Illuminate\Http\Response
     */
    public function destroy(Perros $perros)
    {
        //
    }
}
