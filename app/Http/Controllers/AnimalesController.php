<?php

namespace App\Http\Controllers;

use App\Models\Animales;
use App\Http\Requests\StoreAnimalesRequest;
use App\Http\Requests\UpdateAnimalesRequest;
use App\Models\Favoritos;
use App\Models\User;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;

class AnimalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        $user = User::find($user_id);
        $animals = $user->genero == 0 ? Animales::where('tipo', 0)->get() : Animales::where('tipo', 1)->get();
        return Inertia::render('Animal/Index', ['animals' => $animals, 'user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAnimalesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::id();
        $user = User::find($user_id);

        Log::debug(json_encode($user));

        $request-> validate([
            'img'=>'required',
            'descripcion'=>'required',
        ]);
        $animal = new Animales($request->input());
        $animal->save();
        return redirect('animal');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Animales  $animales
     * @return \Illuminate\Http\Response
     */
    public function show(Animales $animales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Animales  $animales
     * @return \Illuminate\Http\Response
     */
    public function edit(Animales $animales)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAnimalesRequest  $request
     * @param  \App\Models\Animales  $animales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $animal=Animales::find($id);
        $animal->fill($request->input())->saveOrFail();
        return redirect('animal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Animales  $animales
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $animal=Animales::find($id);
        $animal->delete();
        return redirect()->route('animal.index');
    }
}
