<?php

namespace App\Http\Controllers;

use App\Models\Gatos;
use App\Http\Requests\StoreGatosRequest;
use App\Http\Requests\UpdateGatosRequest;

class GatosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gatos = Gatos::all();
        return inertia('Gatos/Index', compact('gatos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreGatosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGatosRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gatos  $gatos
     * @return \Illuminate\Http\Response
     */
    public function show(Gatos $gatos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gatos  $gatos
     * @return \Illuminate\Http\Response
     */
    public function edit(Gatos $gatos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateGatosRequest  $request
     * @param  \App\Models\Gatos  $gatos
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGatosRequest $request, Gatos $gatos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gatos  $gatos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gatos $gatos)
    {
        //
    }
}
