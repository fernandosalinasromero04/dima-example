<?php

use App\Http\Controllers\AnimalesController;
use App\Http\Controllers\GatosController;
use App\Http\Controllers\PerrosController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::resource('animal', AnimalesController::class);

    Route::middleware(['verificar.genero.masculino'])->group(function () {
        Route::get('/perros', [PerrosController::class, 'index'])->name('perros.index');
        Route::post('/perros/{perro}/marcar-favorito', [PerrosController::class, 'marcarFavorito'])->name('perros.marcar-favorito');
    });

    Route::middleware(['verificar.genero.femenino'])->group(function () {
        Route::get('/gatos', [GatosController::class, 'index'])->name('gatos.index');
        //Route::post('/gatos/{gato}/marcar-favorito', [GatosController::class, 'marcarFavorito'])->name('gatos.marcar-favorito');
    });
});

require __DIR__.'/auth.php';
